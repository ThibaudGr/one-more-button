import React from "react";
import styled from 'styled-components';

type Props = {
  text: string
  onClick: () => void
}

const NiceButton = ({ text, onClick }: Props): JSX.Element => (
  <Button onClick={onClick}>
    {text}
  </Button>
);

export default NiceButton;

const Button = styled.button`
  padding: 4px;
`